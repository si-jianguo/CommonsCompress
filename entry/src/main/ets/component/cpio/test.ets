/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import fileio from '@ohos.fileio';
import {
  CpioArchiveEntry,
  CpioArchiveInputStream,
  CpioConstants,
  ArchiveStreamFactory,
  ArchiveOutputStream,
  CharacterSetECI,
  InputStream,
  File,
  OutputStream,
  IOUtils,
  Long
} from '@ohos/commons-compress'
import { GlobalContext } from '../ar/GlobalContext';

@Entry
@Component
export struct CpioTest {
  @State isCompressGArFileShow: boolean = false;
  @State isDeCompressGArShow: boolean = false;
  @State newFolder: string = 'newFolder';
  preTimestamp: number = 0;

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
      Text('cpio相关功能')
        .fontSize(20)
        .margin({ top: 16 })

      Text('点击生成test1.xml')
        .fontSize(16)
        .margin({ top: 32 })
        .padding(8)
        .border({ width: 2, color: '#535353', radius: 6 })
        .onClick((event) => {
          if (!this.isFastClick()) {
            this.generateTextFile()
          }
        })

      if (this.isCompressGArFileShow) {
        Text('点击压缩test1.xml为cpio文件')
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.jsCpioTest()
            }
          })
      }

      if (this.isDeCompressGArShow) {
        Text('点击解压cpio')
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.jsUnCpioTest()
            }
          })
      }
    }
    .width('100%')
    .height('100%')
  }

  aboutToAppear() {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");
      fileio.mkdir(data + '/' + this.newFolder)
        .then((err) => {
        });
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  generateTextFile(): void {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");
      console.log("ppp" + 2)
      const writer = fileio.openSync(data + '/' + this.newFolder + '/test1.xml', 0o102, 0o666);
      fileio.writeSync(writer, "<?xml version = '1.0'?>\n"
      + "<!DOCTYPE connections>\n"
      + "<connections>\n"
      + "</connections>");
      fileio.closeSync(writer);
      AlertDialog.show({ title: '生成成功',
        message: '请查看沙箱路径' + data + '/' + this.newFolder + '/test1.xml',
        confirm: { value: 'OK', action: () => {
          this.isCompressGArFileShow = true
        } }
      })
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  jsCpioTest(): void {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");

      this.testReadLongNamesBSD(data as string)
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  jsUnCpioTest(): void {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");

      this.testUnCompressCpio(data as string)
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  isFastClick(): boolean {
    let timestamp:number = Date.parse(new Date().toString());
    if ((timestamp - this.preTimestamp) > 1500) {
      this.preTimestamp = timestamp;
      return false;
    } else {
      return true;
    }
  }

  testReadLongNamesBSD(data: string): void {
    let output: File = new File(data, this.newFolder + ".cpio");

    let file1: File = new File(data + '/' + this.newFolder, "test1.xml");
    let inputStream1 = new InputStream();
    inputStream1.setFilePath(file1.getPath());

    let out: OutputStream = new OutputStream();
    out.setFilePath(output.getPath());
    let os: ArchiveOutputStream = ArchiveStreamFactory.DEFAULT.createArchiveOutputStream("cpio", out);
    let archiveEntry1: CpioArchiveEntry = new CpioArchiveEntry();
    archiveEntry1.initCpioArchiveEntryNameSize("test1.xml", Long.fromNumber(file1.length()));
    os.putArchiveEntry(archiveEntry1);
    IOUtils.copyStream(inputStream1, os);
    os.closeArchiveEntry();

    os.close();
    out.close();

    AlertDialog.show({ title: '压缩成功',
      message: '请查看沙箱路径 ' + data + '/',
      confirm: { value: 'OK', action: () => {
        this.isDeCompressGArShow = true
      } }
    })
  }

  testUnCompressCpio(data: string) {
    let input: File = new File(data, this.newFolder + '.cpio');
    let input1: InputStream = new InputStream();
    input1.setFilePath(input.getPath());
    let tais = new CpioArchiveInputStream(input1, CpioConstants.BLOCK_SIZE, CharacterSetECI.ASCII.getName());
    let cpioArchiveEntry: CpioArchiveEntry;
    while ((cpioArchiveEntry = tais.getNextCPIOEntry()) != null) {
      let name: string = cpioArchiveEntry.getName();
      let tarFile: File = new File(data + '/' + this.newFolder, name);

      if (name.indexOf('/') != -1) {
        try {
          let splitName: string = name.substring(0, name.lastIndexOf('/'));
          fileio.mkdirSync(data + '/' + this.newFolder + '/' + splitName);
        } catch (err) {
        }
      }

      let fos: OutputStream | null = null;
      try {
        fos = new OutputStream();
        fos.setFilePath(tarFile.getPath())
        let read: number = -1;
        let buffer: Int8Array = new Int8Array(1024);
        while ((read = tais.readBytes(buffer)) != -1) {
          fos!.writeBytesOffset(buffer, 0, read);
        }
      } catch (e) {
        throw (e as Error);
      } finally {
        fos!.close();
      }
    }
    AlertDialog.show({ title: '解压成功',
      message: '请查看沙箱路径' + data + '/' + this.newFolder + '/test1.xml',
      confirm: { value: 'OK', action: () => {
        this.isCompressGArFileShow = true
      } }
    })
  }

  test1(data: string): void {
    let input: File = new File(data, "bla.cpio");
    let input1: InputStream = new InputStream();
    input1.setFilePath(input.getPath());
    let tais = new CpioArchiveInputStream(input1, CpioConstants.BLOCK_SIZE, CharacterSetECI.ASCII.getName());
    let tarArchiveEntry: CpioArchiveEntry = tais.getNextCPIOEntry();
    tarArchiveEntry.getName();
  }
}
