/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import fileio from '@ohos.fileio';
import { Decoder, Encoder, InputStream, OutputStream, Exception, System, Long } from '@ohos/commons-compress';
import { GlobalContext } from '../ar/GlobalContext';

@Entry
@Component
export struct LzmaTest {
  @State isCompressLzmaFileShow: boolean = false;
  @State isDeCompressLzmaShow: boolean = false;
  preTimestamp: number = 0;
  TAG: string = 'lzmaTest----'

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center, justifyContent: FlexAlign.Center }) {
      Text('lzma相关功能')
        .fontSize(20)
        .margin({ top: 16 })


      Text('点击生成test.xml')
        .fontSize(16)
        .margin({ top: 32 })
        .padding(8)
        .border({ width: 2, color: '#535353', radius: 6 })
        .onClick((event) => {
          if (!this.isFastClick()) {
            this.generateTextFile()
          }
        })


      if (this.isCompressLzmaFileShow) {
        Text('点击压缩test.xml生成lzma')
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.testEncoder()
            }
          })
      }

      if (this.isDeCompressLzmaShow) {
        Text('点击解压对应文件')
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.testDecoder()
            }
          })
      }
    }
    .width('100%')
    .height('100%')
  }

  generateTextFile(): void {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");
      const writer = fileio.openSync(data + '/test.xml', 0o102, 0o666);
      fileio.writeSync(writer, "<?xml version = '1.0'?>\n"
      + "<!DOCTYPE connections>\n"
      + "<connections>\n"
      + "</connections>");
      fileio.closeSync(writer);
      AlertDialog.show({ title: '生成成功',
        message: '请查看沙箱路径' + data + '/test1.xml',
        confirm: { value: 'OK', action: () => {
          this.isCompressLzmaFileShow = true
        } }
      })
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  encoder(path:string) {
    console.info(this.TAG + '开始')
    let inputStream: InputStream = new InputStream();
    let outputStream: OutputStream = new OutputStream();
    outputStream.setFilePath(path + '/test.xml.lzma')
    inputStream.setFilePath(path + '/test.xml')
    let stat = fileio.statSync(path + '/test.xml');
    let encoder: Encoder = new Encoder();
    if (!encoder.SetAlgorithm(2))
    return
    if(!encoder.SetDictionarySize(1 << 23))
    return
    if(!encoder.SetNumFastBytes(128))
    return
    if(!encoder.SetMatchFinder(1))
    return
    if(!encoder.SetLcLpPb(3, 0, 2))
    return
    encoder.SetEndMarkerMode(false);
    encoder.WriteCoderProperties(outputStream);
    let fileSize: Long = Long.fromNumber(stat.size);
    for (let i = 0; i < 8; i++) {
      outputStream.write(fileSize.shiftRightUnsigned(8 * i).toInt() & 0xFF);
    }
    encoder.Code(inputStream, outputStream, Long.fromNumber(-1), Long.fromNumber(-1), null);
    outputStream.flush();
    outputStream.close();
    inputStream.close();
    console.info(this.TAG + '结束')
  }

  testEncoder(): void {
    try {
      let path = GlobalContext.getContext().getObject("FilesDir");
      this.encoder(path as string)

      AlertDialog.show({ title: '压缩成功',
        message: '请查看沙箱路径' + path + '/test.xml.lzma',
        confirm: { value: 'OK', action: () => {
          this.isDeCompressLzmaShow = true
        } }
      })
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  decoder(path:string) {
    let decoder: Decoder = new Decoder();
    let inputStream: InputStream = new InputStream();
    inputStream.setFilePath(path + '/test.xml.lzma')
    let outputStream: OutputStream = new OutputStream();
    outputStream.setFilePath(path + '/test.xml')
    let propertiesSize = 5;
    let properties: Int8Array = new Int8Array(propertiesSize);
    if (inputStream.readBytesOffset(properties, 0, propertiesSize) != propertiesSize)
    throw new Exception("input .lzma file is too short");
    if (!decoder.SetDecoderProperties(properties))
    throw new Exception("Incorrect stream properties");
    let outSize: Long = Long.fromNumber(0);
    for (let i = 0; i < 8; i++) {
      let v: number = inputStream.read();
      if (v < 0)
      throw new Exception("Can't read stream size");
      outSize = outSize.or(Long.fromNumber(v).shiftLeft(8 * i));
    }
    if (!decoder.Code(inputStream, outputStream, outSize))
    throw new Exception("Error in data stream");
    outputStream.flush();
    outputStream.close();
    inputStream.close();
  }

  testDecoder(): void {
    try {
      let path = GlobalContext.getContext().getObject("FilesDir");

      let timer1 = System.currentTimeMillis().toString()
      console.info(this.TAG + timer1)

      this.decoder(path as string)

      let timer2: string = System.currentTimeMillis().toString()
      console.info(this.TAG + timer2)

      AlertDialog.show({ title: '解压缩成功',
        message: '请查看沙箱路径' + path + '/test1.xml',
        confirm: { value: 'OK', action: () => {

        } }
      })
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  isFastClick(): boolean {
    let timestamp:number = Date.parse(new Date().toString());
    if ((timestamp - this.preTimestamp) > 1500) {
      this.preTimestamp = timestamp;
      return false;
    } else {
      return true;
    }
  }
}