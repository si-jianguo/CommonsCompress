/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import fileio from '@ohos.fileio';
import { zstdCompress, zstdDecompress } from '@ohos/commons-compress';
import { GlobalContext } from '../ar/GlobalContext';

@Entry
@Component
export struct ZSTDTest {
  @State isCompressZstdFileShow: boolean = false;
  @State isDeCompressZstdShow: boolean = false;
  preTimestamp: number = 0;
  compressedSize: number = 0;
  compressed: Int8Array | null = null;
  original: Int8Array | null = null;

  build() {
    Flex({ direction: FlexDirection.Column, alignItems: ItemAlign.Center }) {
      Text('zstd相关功能')
        .fontSize(20)
        .margin({ top: 16 })

      Text('点击生成hello.txt')
        .fontSize(16)
        .margin({ top: 32 })
        .padding(8)
        .border({ width: 2, color: '#535353', radius: 6 })
        .onClick((event) => {
          if (!this.isFastClick()) {
            this.generateTextFile()
          }
        })

      if (this.isCompressZstdFileShow) {
        Text('点击压缩hello.txt为zstd文件')
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.testZstdCompressed()
            }
          })
      }


      if (this.isDeCompressZstdShow) {
        Text('点击解压zstd文件')
          .fontSize(16)
          .margin({ top: 32 })
          .padding(8)
          .border({ width: 2, color: '#535353', radius: 6 })
          .onClick((event) => {
            if (!this.isFastClick()) {
              this.testZstdDecompressed()
            }
          })
      }
    }
    .width('100%')
    .height('100%')
  }

  generateTextFile(): void {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");
      const writer = fileio.openSync(data + '/hello.txt', 0o102, 0o666);
      fileio.writeSync(writer, "hello, world!  adjasjdakjdakjdkjakjdakjskjasdkjaskjdajksdkjasdkjaksjdkja\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs\n"
      + "adasajsdkjadjkakjdakjsdkjadkjakjdakjsdkjasdkjaskjdakjsdkjaskjdakjsdkjaskjakjdakjs"
      );
      fileio.closeSync(writer);
      AlertDialog.show({ title: '生成成功',
        message: '请查看沙箱路径' + data + '/hello.txt',
        confirm: { value: 'OK', action: () => {
          this.isCompressZstdFileShow = true
        } }
      })
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  async testZstdCompressed(): Promise<void> {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir")
      await zstdCompress(data + "/hello.txt", data + "/hello.txt.zstd", 1, 3).then((value) => {
        if (value) {
          AlertDialog.show({ title: '压缩成功',
            message: '请查看沙箱路径 ' + data + '/hello.txt.zstd',
            confirm: { value: 'OK', action: () => {
              this.isDeCompressZstdShow = true
            } }
          })
        } else {
          AlertDialog.show({ title: '压缩失败',
            message: '请检查再压缩 ',
            confirm: { value: 'OK', action: () => {
            } }
          })
        }
      })
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  async testZstdDecompressed(): Promise<void> {
    try {
      let data = GlobalContext.getContext().getObject("FilesDir");
      await zstdDecompress(data + "/hello.txt.zstd", data + '/newhello.txt').then((value) => {
        if (value) {
          AlertDialog.show({ title: '解压成功',
            message: '请查看沙箱路径 ' + data + '/newhello.txt',
            confirm: { value: 'OK', action: () => {
            } }
          })
        } else {
          AlertDialog.show({ title: '解压失败',
            message: '请检查再解压',
            confirm: { value: 'OK', action: () => {
            } }
          })
        }
      })
    } catch (error) {
      console.error('File to obtain the file directory. Cause: ' + error.message);
    }
  }

  isFastClick(): boolean {
    let timestamp:number = Date.parse(new Date().toString());
    if ((timestamp - this.preTimestamp) > 1500) {
      this.preTimestamp = timestamp;
      return false;
    } else {
      return true;
    }
  }
}


