/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import LZ77CompressorTest from './compressors/lz77/LZ77Compressor.test'
import exampleJsunitGzip from './ExampleJsunitGzip.test'
import arArchiveInputStreamTest from './archivers/ar/arArchiveInputStream.test'
import brotliCompressorInputStreamTest from './compressors/brotli/BrotliCompressorInputStream.test'
import zCompressorInputStreamTest from './compressors/z/ZCompressorInputStream.test'
import xzCompressorInputStreamTest from './compressors/xz/XZCompressorInputStream.test'
import xzCompressorOutputStreamTest from './compressors/xz/XZCompressorOutputStream.test'
import DumpArchiveEntryTest from './archivers/dump/DumpArchiveEntry.test'
import DumpArchiveUtilTest from './archivers/dump/DumpArchiveUtil.test'
import DumpArchiveInputStreamTest from './archivers/dump/DumpArchiveInputStream.test'
import DumpTestCase from './archivers/dump/DumpTestCase.test'
import ExampleJsunitLzma from './ExampleJsunitLzma.test'
import tarCompressorTest from './archivers/tar/tarCompressor.test'
import bzip2CompressorTest from './compressors/bzip2/bzip2Compressor.tset'
import CpioArchiveInputStreamTest from './archivers/cpio/CpioArchiveInputStream.test'
import CpioArchiveOutputStreamTest from './archivers/cpio/CpioArchiveOutputStream.test'
import CpioUtilTest from './archivers/cpio/CpioUtil.test'
import ZSTDCompressorTest from './compressors/zstd/ZSTDCompressor.test'
import DeflateTest from './Deflate.test'
import Lz4Test from './Lz4.test'
import SnappyTest from './snappy.test'
import telephonyPerfJsunit from './TestInterfaceResponseTime.test'

export default function testsuite() {
  LZ77CompressorTest()
  exampleJsunitGzip()
  arArchiveInputStreamTest()
  zCompressorInputStreamTest()
  xzCompressorInputStreamTest()
  xzCompressorOutputStreamTest()
  DumpArchiveEntryTest()
  DumpArchiveUtilTest()
  DumpArchiveInputStreamTest()
  DumpTestCase()
  ExampleJsunitLzma()
  tarCompressorTest()
  bzip2CompressorTest()
  CpioArchiveInputStreamTest()
  CpioArchiveOutputStreamTest()
  CpioUtilTest()
  brotliCompressorInputStreamTest()
  ZSTDCompressorTest()
  DeflateTest()
  Lz4Test()
  SnappyTest()
  telephonyPerfJsunit()
}