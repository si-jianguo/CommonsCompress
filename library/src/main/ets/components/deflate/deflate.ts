/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import fileio from '@ohos.fileio';
import pako from "pako";


export async function DeflateFile(src: string, dest: string): Promise<boolean> {
    try {
        let stat = fileio.statSync(src);
        const buf = new ArrayBuffer(stat.size);
        const reader = fileio.openSync(src, 0o2);
        fileio.readSync(reader, buf);
        const writer = fileio.openSync(dest, 0o102, 0o666);
        const options = { deflate: true, level: 9 };
        fileio.writeSync(writer, pako.deflate(new Uint8Array(buf), options).buffer);
        fileio.closeSync(reader);
        fileio.closeSync(writer);
        return true;
    } catch (error) {
        return false;
    }
}


export async function InflateFile(src: string, target: string): Promise<boolean> {
    try {
        const reader = fileio.openSync(src, 0o2);
        const stat = fileio.statSync(src);
        const buf = new ArrayBuffer(stat.size);
        const res = await fileio.read(reader, buf);
        const options = { deflate: true, level: 9 };
        const data = pako.inflate(new Uint8Array(res.buffer), options);
        const writer = fileio.openSync(target, 0o102, 0o666);
        fileio.writeSync(writer, data.buffer);
        fileio.closeSync(writer);
        fileio.closeSync(reader);
        return true;
    } catch (error) {
        return false;
    }
}