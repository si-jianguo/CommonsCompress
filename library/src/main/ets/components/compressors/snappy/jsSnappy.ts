/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
import snappyJS from 'snappyjs'
import fileio from '@ohos.fileio';

export async function snappyCompress(src: string, dest: string): Promise<boolean> {
  try {
    let buf = getFileBuf(src)
    /* 压缩文件*/
    var compressed = snappyJS.compress(buf)
    let fd = fileio.openSync(dest, 0o102, 0o666);
    let num = await fileio.write(fd, compressed);
    fileio.closeSync(fd);
    return true;
  } catch (error) {
    return false;
  }
}

/* 解压文件*/
export async function snappyUncompress(src: string, target: string): Promise<boolean> {
  try {
    let buf = getFileBuf(src)
    var uncompressed = snappyJS.uncompress(buf)
    let fd = fileio.openSync(target, 0o102, 0o666);
    let num = await fileio.write(fd, uncompressed);
    fileio.closeSync(fd);
    return true;
  } catch (error) {
    return false;
  }
}

function getFileBuf(Bufpath): ArrayBuffer {
  let stat = fileio.statSync(Bufpath);
  const reader = fileio.openSync(Bufpath);
  let buf = new ArrayBuffer(stat.size);
  fileio.readSync(reader, buf);
  fileio.closeSync(reader);
  return buf
}




