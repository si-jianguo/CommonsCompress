/*
 * Copyright (c) 2022 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/**
 * 压缩文件
 * src:需要压缩的文件包路径;
 * archive:压缩文件存放的路径;
 * level:压缩等级;
 * nbThreads:线程数,默认为3
 * number:返回值，压缩成功则返回压缩字节，否则返回失败;
 */
export const zstdCompress: (src: string, archive: string, level: number, nbThreads: number) => Promise<number>;
/**
 * 解压文件
 * archive:需要解压的文件包路径;
 * dest:解压文件存放的路径;
 * number:返回值，压缩成功则返回解压字节，否则返回失败;
 */
export const zstdDecompress: (archive: string, dest: string) => Promise<number>;